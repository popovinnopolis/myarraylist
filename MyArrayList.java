package MyArrayList;

import java.util.NoSuchElementException;

/**
 * Created by evgenijpopov on 07.06.17.
 */
public class MyArrayList<T> {
    private static final int defaultCapacity = 10;
    private int capacity;
    private int size = 0;
    private Object[] vault;

    public MyArrayList(int capacity) {
        this.capacity = capacity;
        this.vault = new Object[capacity];
    }

    public MyArrayList() {
        this(defaultCapacity);
    }

    public void Add(T item) {
        if (item == null) return;
        if (size >= capacity) {
            capacity = 3 * capacity / 2 + 1;
            Object[] tmpVault = new Object[capacity];
            System.arraycopy(vault, 0, tmpVault, 0, size);
            vault = tmpVault;
        }
        vault[size] = item;
        size++;
    }

    public void AddAt(T item, int index) {
        if (item == null) return;
        if (size >= capacity) {
            capacity = 3 * capacity / 2 + 1;
            Object[] tmpVault = new Object[capacity];
            System.arraycopy(vault, 0, tmpVault, 0, size);
            vault = tmpVault;
        }
        System.arraycopy(vault, index, vault, index+1, size - index);
        vault[index] = item;
        size++;
    }

    public void Remove(T item) {
        if (item == null) return;
        boolean res = false;

        for (int i = size - 1; i > -1; i--) {
            T currItem = (T) vault[i];
            if (currItem.equals(item)) {
                System.arraycopy(vault, i + 1, vault, i, size - i - 1);
                vault[--size] = null;
                res = true;
            }
        }
        if (!res) throw new NoSuchElementException();
    }

    public void RemoveAt(int index) {
        if (size < index) throw new IndexOutOfBoundsException();
        System.arraycopy(vault, index + 1, vault, index, size - index - 1);
        vault[size--] = null;
    }

    public boolean Contains(T item) {
        for (int i = 0; i < size; i++) {
            T currItem = (T) vault[i];
            if (currItem.equals(item)) return true;
        }
        return false;
    }

    public String toString() {
        if (size == 0) return "";
        StringBuilder res = new StringBuilder();
        res.append("[");
        for (int i = 0; i < size; i++) {
            T currItem = (T) vault[i];
            res.append(currItem);
            if (i < size - 1) res.append(", ");
        }
        res.append("]");
        return res.toString();
    }

}
