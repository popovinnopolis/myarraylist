package MyArrayList;


/**
 * Created by evgenijpopov on 07.06.17.
 */
public class TestMyArrayList {
    public static void main(String[] args) {
        MyArrayList<Integer> a = new MyArrayList<Integer>(3);
        System.out.println("Initial state: " + a);
        a.Add(1);
        a.Add(2);
        a.Add(1);
        a.Add(3);
        a.Add(4);
        a.Add(1);
        a.Add(1);
        System.out.println("After some insertion: " + a);
        System.out.println("Is contains 3: " + a.Contains(3));
        System.out.println("Is contains 33: " + a.Contains(33));

        a.AddAt(22, 2);
        System.out.println("After insert 22 at position 2: " + a);

        a.Remove(1);
        System.out.println("After remove 1: " + a);

        a.RemoveAt(1);
        System.out.println("After removeAt 1: " + a);
    }
}
